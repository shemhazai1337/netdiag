from setuptools import setup, find_packages

setup(name='netdiag',
      version='1.3',
      description="""
NetDiag is a network diagnostic and information gatherer tool.
""",
      url='https://bitbucket.org/itslikeme/netdiag',
      author='Shemhazai',
      author_email='nestorm2486@gmail.com',
      license='MIT',
      scripts=['bin/netdiag'],
      zip_safe=False)
